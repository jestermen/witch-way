package  
{
	import flash.display.MovieClip;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	
	import caurina.transitions.Tweener;
	
	/**
	 * <p>Document class for "Witch Way".</p>
	 * 
	 * @author Adam Testerman
	 */
	public class WWMain extends MovieClip 
	{
		//PRIMITIVES
		private var _defaultText:String;
		
		//LISTS
		private var _yesAnswers:Array = 			[	"YES.", 
														"MOST CERTAINLY.", 
														"NO DOUBT ABOUT IT.",
														"SORT OF." ];
		private var _noAnswers:Array = 				[	"NO.", 
														"NOT REALLY.", 
														"NOT EXACTLY.", 
														"DEFINITELY NOT." ];
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>WWMain</code>.
		 */
		public function WWMain():void 
		{
			//New methods
			AddEventListeners();
			
			//Inherited properties
			txtQuestionInput.visible = false;
			txtQuestionInput.alpha = 0;
			answerClip.visible = false;
			answerClip.alpha = 0;
			answerClip.txtAnswerOutput.text = "";
			btnAsk.alpha = 0;
			btnAskAgain.alpha = 0;
			
			//Inherited methods
			txtQuestionInput.replaceText(txtQuestionInput.length - 1, txtQuestionInput.length, "");	//removes the janky carriage return at the end of the text field
			_defaultText = txtQuestionInput.text;
		}
		
		
		/**
		 * Adds all event listeners that work with the entirety of this object.
		 */
		private function AddEventListeners():void 
		{
			btnPlay.addEventListener(MouseEvent.CLICK, HandleStartClicked);
			btnAsk.addEventListener(MouseEvent.CLICK, HandleAskClicked);
			btnAskAgain.addEventListener(MouseEvent.CLICK, HandleStartClicked);
			txtQuestionInput.addEventListener(FocusEvent.FOCUS_IN, HandleQuestionInputFocusedIn);
			txtQuestionInput.addEventListener(FocusEvent.FOCUS_OUT, HandleQuestionInputFocusedOut);
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			btnPlay.removeEventListener(MouseEvent.CLICK, HandleStartClicked);
			btnAsk.removeEventListener(MouseEvent.CLICK, HandleAskClicked);
			btnAskAgain.removeEventListener(MouseEvent.CLICK, HandleStartClicked);
			txtQuestionInput.removeEventListener(FocusEvent.FOCUS_IN, HandleQuestionInputFocusedIn);
			txtQuestionInput.removeEventListener(FocusEvent.FOCUS_OUT, HandleQuestionInputFocusedOut);
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		/**
		 * Called when the user clicks the Ask button. Randomly chooses an answer and displays it on the screen. Then allows the user to ask another question.
		 */
		private function HandleAskClicked(event:MouseEvent):void 
		{
			if (txtQuestionInput.text != "" && txtQuestionInput.text != _defaultText)
			{
				//Choose between yes or no
				var answerPool:Array;
				if (Math.random() < .5)
				{
					//YES
					answerPool = _yesAnswers;
				}
				else
				{
					//NO
					answerPool = _noAnswers;
				}
				
				//Choose random response from within answer pool
				var answer:String = answerPool[int(Math.random() * answerPool.length)];
				
				//Show answer
				answerClip.visible = true;
				answerClip.alpha = 0;
				answerClip.txtAnswerOutput.text = answer;
				Tweener.addTween(answerClip, { alpha:1, time:1, transition:"linear" } );
				
				//Hide/disable Ask button
				btnAsk.removeEventListener(MouseEvent.CLICK, HandleAskClicked);
				btnAsk.mouseEnabled = false;
				Tweener.addTween(btnAsk, { alpha:0, time:1, transition:"linear" } );
				
				//Show/enable Ask Again button
				btnAskAgain.addEventListener(MouseEvent.CLICK, HandleStartClicked);
				btnAskAgain.mouseEnabled = true;
				btnAskAgain.visible = true;
				btnAskAgain.alpha = 0;
				Tweener.addTween(btnAskAgain, { alpha:1, time:1, transition:"linear" } );
			}
		}
		
		/**
		 * Called when the user sets focus to the question input text field, either by clicking on it or tabbing to it. As long as the current text of the text field is
		 * not the default text, the entire text field will be cleared so the user can type their question.
		 */
		private function HandleQuestionInputFocusedIn(event:FocusEvent):void 
		{
			if (txtQuestionInput.text == _defaultText) txtQuestionInput.text = "";
		}
		
		/**
		 * Called when the question input text field loses focus. Sets the text of the text field to the default text only if the text of the text field is blank.
		 */
		private function HandleQuestionInputFocusedOut(event:FocusEvent):void 
		{
			if (txtQuestionInput.text == "") txtQuestionInput.text = _defaultText;
		}
		
		/**
		 * Called when the user clicks the Play button. Tweens out the Play button and the title text and tweens in the question prompt and question mark button.
		 */
		private function HandleStartClicked(event:MouseEvent):void 
		{
			//Hide/disable Play button
			btnPlay.removeEventListener(MouseEvent.CLICK, HandleStartClicked);
			btnPlay.mouseEnabled = false;
			Tweener.addTween(btnPlay, { alpha:0, time:1, transition:"linear" } );
			
			//Hide/disable Ask Again button
			btnAskAgain.removeEventListener(MouseEvent.CLICK, HandleStartClicked);
			btnAskAgain.mouseEnabled = false;
			Tweener.addTween(btnAskAgain, { alpha:0, time:1, transition:"linear" } );
			
			//Hide/disable answer
			//answerClip.visible = true;
			//answerClip.alpha = 1;
			Tweener.addTween(answerClip, { alpha:0, time:1, transition:"linear" } );
			
			//Show/enable Ask button
			btnAsk.addEventListener(MouseEvent.CLICK, HandleAskClicked);
			btnAsk.mouseEnabled = true;
			btnAsk.visible = true;
			btnAsk.alpha = 0;
			Tweener.addTween(btnAsk, { alpha:1, time:1, transition:"linear" } );
			
			//Show/enable question input
			txtQuestionInput.mouseEnabled = true;
			txtQuestionInput.visible = true;
			txtQuestionInput.alpha = 0;
			txtQuestionInput.text = _defaultText;
			Tweener.addTween(txtQuestionInput, { alpha:1, time:1, transition:"linear" } );
		}
	}
}