﻿package com.jestermen.witchWay 
{
	import caurina.transitions.Tweener;
	import com.jestermen.interfaces.IObservable;
	import com.jestermen.managers.ObserverManager;
	import com.jestermen.utils.SoundEngine;
	import com.jestermen.witchWay.events.WitchWayEvent;
	import com.jestermen.witchWay.managers.AssetManager;
	import com.jestermen.witchWay.ui.screens.Screen;
	import com.jestermen.witchWay.ui.screens.ScreenAbout;
	import com.jestermen.witchWay.ui.screens.ScreenCustomAnswers;
	import com.jestermen.witchWay.ui.screens.ScreenGame;
	import com.jestermen.witchWay.ui.screens.ScreenHowTo;
	import com.jestermen.witchWay.ui.screens.ScreenMainMenu;
	import com.jestermen.witchWay.ui.screens.ScreenOptions;
	import com.jestermen.witchWay.ui.screens.ScreenSplash;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * Document class for "Witch Way."
	 * 
	 * @author Adam Testerman
	 */
	public class WitchWay extends Sprite implements IObservable
	{
		//PRIMITIVES
		private var _parallaxStartX:int; 		//starting position of the parallax bg; used for transitioning in
		
		//OBJECTS
		private var _parallaxBG:Image;
		private var _se:SoundEngine = SoundEngine.getInstance();
		
		//GAME SCREENS
		private var _screenAbout:ScreenAbout;
		private var _screenCustomAnswers:ScreenCustomAnswers;
		private var _screenGame:ScreenGame;
		private var _screenHowTo:ScreenHowTo;
		private var _screenMainMenu:ScreenMainMenu;
		private var _screenOptions:ScreenOptions;
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>WitchWay</code>.
		 */
		public function WitchWay():void 
		{
			stage ? init() : addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes the application. Should only be called after the application has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			ObserverManager.subscribe(this);
			
			AssetManager.init();
			
			Global.witchAnswers = AssetManager.witchDefaultAnswers;
			
			LoadWitchAnswers();
			
			//Create and display the background images
			var staticBG:Image = new Image(AssetManager.bgStatic);
			staticBG.x = stage.stageWidth / 2 - staticBG.width / 2; //center horizontally
			staticBG.y = stage.stageHeight - staticBG.height;
			staticBG.touchable = false;
			staticBG.blendMode = BlendMode.NONE;
			addChild(staticBG);
			
			_parallaxBG = new Image(AssetManager.bgParallax);
			_parallaxBG.x = _parallaxStartX = -_parallaxBG.width / 6;
			_parallaxBG.y = stage.stageHeight - _parallaxBG.height;
			_parallaxBG.touchable = false;
			addChild(_parallaxBG);
			
			ShowScreenMainMenu();
			
			//Create and display the splash screen (will remove itself when finished)
			var screenSplash:ScreenSplash = new ScreenSplash();
			addChild(screenSplash);
		}
		
		
		//=================================
		//		INTERFACE METHODS
		//=================================
		
		/**
		 * Checks to see if the specified notification is relevant to this object's interests and acts accordingly.
		 */
		public function update(notification:String, notifier:* = null, data:* = null):void 
		{
			/**
			 * User triggered the back button on one of the screens.
			 * 
			 * Returns to the previous screen, transitioning the parallax background, the previous screen, and the screen that corresponds to the triggered event.
			 */
			if (notification == WitchWayEvent.USER_TRIGGERED_SCREEN_BACK)
			{
				//If the custom answers screen triggered this event, save the witch's answers and return to the options screen
				if (notifier is ScreenCustomAnswers)
				{
					SaveWitchAnswers();
					
					ShowScreenOptions();
					TweenScreen(_screenOptions, 0);
					TweenScreen(_screenCustomAnswers, stage.stageWidth);
					Tweener.addTween(_parallaxBG, { x: -_parallaxBG.width / 3, time:Config.SCREEN_TRANSITION_TIME } );
				}
				//Otherwise, we know we're returning to the main menu
				else
				{
					ShowScreenMainMenu();
					TweenScreen(_screenMainMenu, 0);
					TweenScreen(notifier, notifier.startX);
					Tweener.addTween(_parallaxBG, { x: -_parallaxBG.width / 6, time:Config.SCREEN_TRANSITION_TIME } );
				}
			}
			/**
			 * User triggered the play button on the main menu screen.
			 * 
			 * Enters the game screen, transitioning the parallax background, the main menu, and the screen that corresponds to the triggered event.
			 */
			else if (notification == WitchWayEvent.USER_TRIGGERED_MAIN_PLAY)
			{
				//Hide all unneeded elements
				TweenScreen(_screenMainMenu, -stage.stageWidth, HideScreenMainMenu);
				HideScreenHowTo();
				HideScreenOptions();
				HideScreenAbout();
				HideScreenCustomAnswers();
				
				//Show all needed elements
				ShowScreenGame();
				TweenScreen(_screenGame, 0);
				Tweener.addTween(_parallaxBG, { x:-_parallaxBG.width / 3, time:Config.SCREEN_TRANSITION_TIME } );
			}
			/**
			 * User triggered the how to button on the main menu screen.
			 * 
			 * Enters the how to screen, transitioning the parallax background, the main menu, and the screen that corresponds to the triggered event.
			 */
			else if (notification == WitchWayEvent.USER_TRIGGERED_MAIN_HOW_TO)
			{
				//Hide all unneeded elements
				TweenScreen(_screenMainMenu, stage.stageWidth, HideScreenMainMenu);
				HideScreenGame();
				HideScreenOptions();
				HideScreenAbout();
				HideScreenCustomAnswers();
				
				//Show all needed elements
				ShowScreenHowTo();
				TweenScreen(_screenHowTo, 0);
				Tweener.addTween(_parallaxBG, { x:0, time:Config.SCREEN_TRANSITION_TIME } );
			}
			/**
			 * User triggered the options button on the main menu screen.
			 * 
			 * Enters the options screen, transitioning the parallax background, the main menu, and the screen that corresponds to the triggered event.
			 */
			else if (notification == WitchWayEvent.USER_TRIGGERED_MAIN_OPTIONS)
			{
				//Hide all unneeded elements
				TweenScreen(_screenMainMenu, -stage.stageWidth, HideScreenMainMenu);
				HideScreenGame();
				HideScreenHowTo();
				HideScreenAbout();
				HideScreenCustomAnswers();
				
				//Show all needed elements
				ShowScreenOptions();
				TweenScreen(_screenOptions, 0);
				Tweener.addTween(_parallaxBG, { x:-_parallaxBG.width / 3, time:Config.SCREEN_TRANSITION_TIME } );
			}
			/**
			 * User triggered the about button on the main menu screen.
			 * 
			 * Enters the about screen, transitioning the parallax background, the main menu, and the screen that corresponds to the triggered event.
			 */
			else if (notification == WitchWayEvent.USER_TRIGGERED_MAIN_ABOUT)
			{
				//Hide all unneeded elements
				TweenScreen(_screenMainMenu, stage.stageWidth, HideScreenMainMenu);
				HideScreenGame();
				HideScreenHowTo();
				HideScreenOptions();
				HideScreenCustomAnswers();
				
				//Show all needed elements
				ShowScreenAbout();
				TweenScreen(_screenAbout, 0);
				Tweener.addTween(_parallaxBG, { x:0, time:Config.SCREEN_TRANSITION_TIME } );
			}
			/**
			 * User triggerd the custom answer button on the options screen.
			 * 
			 * Enters the custom answers screen, transitioning the parallax background, the options screen, and the screen that corresponds to the triggered event.
			 */
			else if (notification == WitchWayEvent.USER_TRIGGERED_OPTIONS_CUSTOM_ANSWERS)
			{
				//Hide all unneeded elements
				TweenScreen(_screenOptions, -stage.stageWidth, HideScreenOptions);
				HideScreenGame();
				HideScreenHowTo();
				HideScreenMainMenu();
				HideScreenAbout();
				
				//Show all needed elements
				ShowScreenCustomAnswers();
				TweenScreen(_screenCustomAnswers, 0);
				Tweener.addTween(_parallaxBG, { x:-_parallaxBG.width / 2, time:Config.SCREEN_TRANSITION_TIME } );
			}
		}
		
		
		//=================================
		//		LOAD/SAVE WITCH ANSWERS
		//=================================
		
		/**
		 * Opens the file that contains the witch's answers and caches the data. If the file doesn't exist, it will be created and saved in the 
		 * applicationStorageDirectory.
		 */
		private function LoadWitchAnswers():void 
		{
			//Get the correct path
			var file:File = File.applicationStorageDirectory.resolvePath("ans.xml");
			
			//If the answers file already exists, just open it
			if (file.exists)
			{
				var fileStream:FileStream = new FileStream();
				fileStream.open(file, FileMode.READ);
				Global.witchAnswers = XML(fileStream.readUTFBytes(file.size));
				Global.isUsingCustomAnswersOnly = (Global.witchAnswers.@isUsingCustomAnswersOnly == "true");	//read whether using custom answers only
				fileStream.close();
				
				//trace("loaded: " + witchAnswers);
			}
			//Otherwise, create it and save it
			else
			{
				trace("WitchWay.LoadWitchAnswers(): File does not exist! Saving a copy of the default answers.");
				
				SaveWitchAnswers();
			}
		}
		
		/**
		 * Writes the witch's answers to disk, including any custom answers created by the user.
		 */
		private function SaveWitchAnswers():void 
		{
			Global.witchAnswers.@isUsingCustomAnswersOnly = String(Global.isUsingCustomAnswersOnly);
			
			var file:File = File.applicationStorageDirectory.resolvePath("ans.xml");
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeUTFBytes(String(Global.witchAnswers));
			fileStream.close();
			
			trace("WitchWay.SaveWitchAnswers(): Save complete.");
		}
		
		
		//=================================
		//		GAME SCREENS
		//=================================
		
		/**
		 * Tweens the specified game screen to the specified x-coordinate.
		 * 
		 * @param toX The x-position to tween the screen to.
		 * @param onComplete The optional function that will be called when the tween has completed.
		 */
		private function TweenScreen(gameScreen:Screen, toX:int, onComplete:Function = null):void 
		{
			var tweenParams:Object = { };
			tweenParams.x = toX;
			tweenParams.time = Config.SCREEN_TRANSITION_TIME;
			if (gameScreen is ScreenCustomAnswers) 
			{
				tweenParams.onUpdate = ScreenCustomAnswers(gameScreen).updateDisplay;	//to deal with the input text field while tweening
			}
			if (onComplete) 
			{
				tweenParams.onComplete = onComplete;
			}
			
			Tweener.addTween(gameScreen, tweenParams);
		}
		
		private function ShowScreenMainMenu():void 
		{
			if (!_screenMainMenu) _screenMainMenu = new ScreenMainMenu();
			addChild(_screenMainMenu);
			_screenMainMenu.visible = true;
		}
		
		private function HideScreenMainMenu():void 
		{
			if (_screenMainMenu)
			{
				_screenMainMenu.visible = false;
			}
		}
		
		private function ShowScreenGame():void 
		{
			if (!_screenGame) _screenGame = new ScreenGame();
			addChild(_screenGame);
			_screenGame.visible = true;
			
			_se.playSound(Config.SFX_BUBBLE, 0, -1);
		}
		
		private function HideScreenGame():void 
		{
			if (_screenGame)
			{
				_screenGame.visible = false;
			}
		}
		
		private function ShowScreenHowTo():void 
		{
			if (!_screenHowTo) _screenHowTo = new ScreenHowTo();
			addChild(_screenHowTo);
			_screenHowTo.visible = true;
		}
		
		private function HideScreenHowTo():void 
		{
			if (_screenHowTo)
			{
				_screenHowTo.visible = false;
			}
		}
		
		private function ShowScreenOptions():void 
		{
			if (!_screenOptions) _screenOptions = new ScreenOptions();
			addChild(_screenOptions);
			_screenOptions.visible = true;
		}
		
		private function HideScreenOptions():void 
		{
			if (_screenOptions)
			{
				_screenOptions.visible = false;
			}
		}
		
		private function ShowScreenAbout():void 
		{
			if (!_screenAbout) _screenAbout = new ScreenAbout();
			addChild(_screenAbout);
			_screenAbout.visible = true;
		}
		
		private function HideScreenAbout():void 
		{
			if (_screenAbout)
			{
				_screenAbout.visible = false;
			}
		}
		
		private function ShowScreenCustomAnswers():void 
		{
			if (!_screenCustomAnswers) _screenCustomAnswers = new ScreenCustomAnswers();
			addChild(_screenCustomAnswers);
			_screenCustomAnswers.visible = true;
		}
		
		private function HideScreenCustomAnswers():void 
		{
			if (_screenCustomAnswers)
			{
				_screenCustomAnswers.visible = false;
			}
		}
	}
}