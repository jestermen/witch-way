package com.jestermen.witchWay 
{
	/**
	 * Holds globally-accessible configuration parameters for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class Config 
	{
		//CONSTANTS
		public static const FRAME_WITCH_RESPONSE:int =					17;		//number of frames before witch's response is triggered after her animation starts (INDEPENDENT OF STAGE FPS)
		public static const FRAME_WITCH_STIR_END:int =					24;		//number of frames of entire witch/steam animation (INDEPENDENT OF STAGE FPS)		
		public static const SCREEN_TRANSITION_TIME:Number =				1;		//time, in seconds, it takes for screens to transition in and out of visibility
		public static const ANSWER_TRANSITION_TIME:Number =				.5;		//time, in seconds, it takes for the witch's answer to fade in
		public static const SPLASH_TIMER_TEXT:int =						120;	//number of frames before the text on the splash screen begins fading out (dependent on stage fps)
		public static const SPLASH_TIMER_ALL:int =						180;	//number of frames before the entire splash screen begins fading out (dependent on stage fps)
		public static const SPLASH_TIMER_END:int =						240;	//number of frames before splash screen is completely gone (dependent on stage fps)
		public static const WITCH_ANIM_FPS:int = 						15;
		
		//CONSTANTS (Audio Labels)
		public static const SFX_BUBBLE:String =							"Bubbles";
		public static const SFX_CACKLE:String =							"Cackle";
		public static const SFX_WHISTLING_CACKLE:String =				"WhistlingCackle";
	}
}