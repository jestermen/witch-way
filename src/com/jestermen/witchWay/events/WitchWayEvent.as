package com.jestermen.witchWay.events 
{
	/**
	 * Contains different event types for "Witch Way."
	 * 
	 * @author Adam Testerman
	 */
	public class WitchWayEvent 
	{
		//CONSTANTS
		public static const USER_TRIGGERED_SCREEN_BACK:String =					"triggeredScreenBack";
		
		public static const USER_TRIGGERED_MAIN_PLAY:String =					"triggeredMainPlay";
		public static const USER_TRIGGERED_MAIN_HOW_TO:String =					"triggeredMainHowTo";
		public static const USER_TRIGGERED_MAIN_OPTIONS:String =				"triggeredMainOptions";
		public static const USER_TRIGGERED_MAIN_ABOUT:String =					"triggeredMainAbout";
		
		public static const USER_TRIGGERED_OPTIONS_CUSTOM_ANSWERS:String =		"triggeredCustomAnswersOptions";
	}
}