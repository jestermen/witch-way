package com.jestermen.witchWay.managers 
{
	import com.emibap.textureAtlas.DynamicAtlas;
	import flash.display.Bitmap;
	import spine.atlas.Atlas;
	import spine.attachments.AtlasAttachmentLoader;
	import spine.SkeletonData;
	import spine.SkeletonJson;
	import spine.starling.SkeletonAnimation;
	import spine.starling.StarlingTextureLoader;
	import starling.core.Starling;
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	/**
	 * This class initializes all of the assets for "Witch Way."
	 * 
	 * Graphical assets are taken from embedded images and XML and converted to texture atlases.
	 * 
	 * Any time the application needs an asset, the asset will be taken from this class, ensuring that assets are never duplicated or created 
	 * elsewhere.
	 * 
	 * @author Adam Testerman
	 */
	public class AssetManager 
	{
		//EMBEDS
		[Embed(source="../../../../../assets/xml/witch-stir-0-15.xml", mimeType="application/octet-stream")]
		private static var WitchStir015XML:Class;
		
		[Embed(source="../../../../../assets/xml/witch-stir-16-23.xml", mimeType="application/octet-stream")]
		private static var WitchStir1623XML:Class;
		
		[Embed(source="../../../../../assets/xml/witch-orb-0-15.xml", mimeType="application/octet-stream")]
		private static var WitchOrb015XML:Class;
		
		[Embed(source="../../../../../assets/xml/witch-default-answers.xml", mimeType="application/octet-stream")]
		private static var WitchDefaultAnswersXML:Class;
		
		[Embed(source="../../../../../assets/fonts/zg.ttf", embedAsCFF="false", fontFamily="Zombie Guts")]
		private static const ZombieGuts:Class;
		
		[Embed(source="../../../../../assets/xml/fire.pex", mimeType="application/octet-stream")]
		private static var FireConfig:Class;
		
		[Embed(source="../../../../../assets/images/fire_particle.png")]
		private static var FireParticle:Class;
		
		[Embed(source = "../../../../../assets/xml/witch.atlas", mimeType = "application/octet-stream")]
		private static var WitchAtlas:Class;
		
		[Embed(source = "../../../../../assets/images/witch.png")]
		public static const WitchAtlasTexture:Class;
		
		[Embed(source = "../../../../../assets/xml/witch.json", mimeType = "application/octet-stream")]
		public static const WitchJson:Class;
		
		//ATLASES
		private static var _witchDefaultAnswers:XML;
		
		//TEXTURES
		public static var bgParallax:Texture;
		public static var bgStatic:Texture;
		public static var check:Texture;
		public static var checkBox:Texture;
		public static var customAnswerAdd:Texture;
		public static var customAnswerRemove:Texture;
		public static var iconSoundOff:Texture;
		public static var jestermenText:Texture;
		public static var scrollBody:Texture;
		public static var scrollBottom:Texture;
		public static var scrollCustomOnlyText:Texture;
		public static var scrollHeaderText:Texture;
		public static var scrollInputTextBorder:Texture;
		public static var scrollTop:Texture;
		public static var signAbout:Texture;
		public static var signAsk:Texture;
		public static var signBack:Texture;
		public static var signCustomAnswers:Texture;
		public static var signHowTo:Texture;
		public static var signMainMenu:Texture;
		public static var signOptions:Texture;
		public static var signReset:Texture;
		public static var signToggleSound:Texture;
		public static var solidGray:Texture;
		public static var solidLightGray:Texture;
		
		//OBJECTS
		public static var particleSystem:PDParticleSystem;
		public static var skeleton:SkeletonAnimation;
		
		//LISTS
		public static var witchStir:Vector.<Texture>;			//for use in witch animations
		public static var witchOrbExplode:Vector.<Texture>;
		public static var witchOrbRoil:Vector.<Texture>;
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Initializes all assets for "Witch Way."
		 */
		public static function init():void 
		{
			//WITCH STIR
			var witchStirAtlas1XML:XML = XML(new WitchStir015XML());
			var witchStirAtlas2XML:XML = XML(new WitchStir1623XML());
			var witchStirAtlas1:TextureAtlas = new TextureAtlas(Texture.fromBitmap(new Bitmap(new WitchAtlas1()), false), witchStirAtlas1XML); //stir animation is too large for one texture atlas; combine both atlases to create the final group of textures for use in a movie clip
			var witchStirAtlas2:TextureAtlas = new TextureAtlas(Texture.fromBitmap(new Bitmap(new WitchAtlas2()), false), witchStirAtlas2XML);
			var witchStirTextures1:Vector.<Texture> = witchStirAtlas1.getTextures("witch-stir-");
			var witchStirTextures2:Vector.<Texture> = witchStirAtlas2.getTextures("witch-stir-");
			witchStir = witchStirTextures1.concat(witchStirTextures2.concat(witchStirTextures1.concat(witchStirTextures2)));	//double the frames so the witch can cycle the anim once
			
			//WITCH ORB
			var witchOrbAtlasXML:XML = XML(new WitchOrb015XML());
			var witchOrbAtlas:TextureAtlas = new TextureAtlas(Texture.fromBitmap(new Bitmap(new WitchOrbAtlas()), false), witchOrbAtlasXML);
			var witchOrbRoilingTextures1:Vector.<Texture> = witchOrbAtlas.getTextures("orb-roil-");
			var witchOrbRoilingTextures2:Vector.<Texture> = witchOrbAtlas.getTextures("orb-roil-");
			var witchOrbRoilingTextures3:Vector.<Texture> = witchOrbAtlas.getTextures("orb-implode-");
			witchOrbRoil = witchOrbRoilingTextures1.concat(witchOrbRoilingTextures2.concat(witchOrbRoilingTextures3));	//two roil cycles and the implode make up the first part of the animation
			witchOrbExplode = witchOrbAtlas.getTextures("orb-explode-");												//one explode cycle makes up the last part of the animation
			
			//PARTICLES
			particleSystem = new PDParticleSystem(XML(new FireConfig()), Texture.fromBitmap(new FireParticle()));
			Starling.juggler.add(AssetManager.particleSystem);
			
			//OTHER TEXTURES
			var standardAssetAtlas1:TextureAtlas = DynamicAtlas.fromMovieClipContainer(new AssetContainer1());
			var standardAssetAtlas2:TextureAtlas = DynamicAtlas.fromMovieClipContainer(new AssetContainer2());
			bgParallax = Texture.fromBitmapData(new ParallaxBG(), false);
			bgStatic = standardAssetAtlas2.getTextures("staticBG")[0];
			check = standardAssetAtlas1.getTextures("check")[0];
			checkBox = standardAssetAtlas1.getTextures("checkBox")[0];
			customAnswerAdd = standardAssetAtlas1.getTextures("customAnswerAdd")[0];
			customAnswerRemove = standardAssetAtlas1.getTextures("customAnswerRemove")[0];
			iconSoundOff = standardAssetAtlas1.getTextures("iconSoundOff")[0];
			jestermenText = standardAssetAtlas1.getTextures("jestermenText")[0];
			scrollBody = standardAssetAtlas2.getTextures("scrollBody")[0];
			scrollBottom = standardAssetAtlas1.getTextures("scrollBottom")[0];
			scrollCustomOnlyText = standardAssetAtlas1.getTextures("scrollCustomOnlyText")[0];
			scrollHeaderText = standardAssetAtlas1.getTextures("scrollHeaderText")[0];
			scrollInputTextBorder = standardAssetAtlas1.getTextures("scrollInputTextBorder")[0];
			scrollTop = standardAssetAtlas1.getTextures("scrollTop")[0];
			signAbout = standardAssetAtlas2.getTextures("signAbout")[0];
			signAsk = standardAssetAtlas1.getTextures("signAsk")[0];
			signBack = standardAssetAtlas1.getTextures("signBack")[0];
			signCustomAnswers = standardAssetAtlas1.getTextures("signCustomAnswers")[0];
			signHowTo = standardAssetAtlas2.getTextures("signHowTo")[0];
			signMainMenu = standardAssetAtlas2.getTextures("signMainMenu")[0];
			signOptions = standardAssetAtlas2.getTextures("signOptions")[0];
			signReset = standardAssetAtlas1.getTextures("signReset")[0];
			signToggleSound = standardAssetAtlas1.getTextures("signToggleSound")[0];
			solidGray = standardAssetAtlas1.getTextures("solidGray")[0];
			solidLightGray = standardAssetAtlas1.getTextures("solidLightGray")[0];
			
			
			
			var atlas:Atlas = new Atlas(new WitchAtlas(), new StarlingTextureLoader(new WitchAtlasTexture()));
			var json:SkeletonJson = new SkeletonJson(new AtlasAttachmentLoader(atlas));
			var skeletonData:SkeletonData = json.readSkeletonData(new WitchJson());
			
			skeleton = new SkeletonAnimation(skeletonData, true);
			skeleton.x = 320;
			skeleton.y = 420;
			skeleton.skeleton.skinName = "default";
			skeleton.skeleton.setSlotsToSetupPose();
			skeleton.state.setAnimationByName(0, "stir", true);
			
			Starling.juggler.add(skeleton);
		}
		
		
		//=================================
		//		ACCESSORS
		//=================================
		
		public static function get witchDefaultAnswers():XML 
		{
			if (!_witchDefaultAnswers) _witchDefaultAnswers = XML(new WitchDefaultAnswersXML());
			
			return _witchDefaultAnswers;
		}
	}
}