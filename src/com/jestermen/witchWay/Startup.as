package com.jestermen.witchWay 
{
	import com.jestermen.utils.SoundEngine;
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.media.AudioPlaybackMode;
	import flash.media.SoundMixer;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import starling.core.Starling;
	import starling.events.ResizeEvent;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	/**
	 * Starling startup class for "Witch Way."
	 * 
	 * In addition to initializing Starling, this class optimizes the display for a 3:4 (768x1024) screen aspect ratio. 
	 * 
	 * - There are four rectangles to keep in mind when looking at how the view is rendered: Flash's stage dimensions, Starling's stage dimensions, Starling's viewport
	 * 		dimensions, and the visible area dimensions. An easy way to think about these rectangles is to imagine a movie clip being placed onto Flash's stage. Inside
	 * 		the movie clip is a single rectangle of any size. The rectangle inside the movie clip is Starling's stage. The movie clip itself is the viewport. So we can
	 * 		change the movie clip's width and height without modifying the width and height of the rectangle inside it. Of course the rectangle will appear stretched, 
	 * 		but its actual width and height do not change: they are simply rendered to fill the viewport. Finally, the visible area is how much of that movie clip can
	 * 		actually be seen, using dimensions relative to the rectangle inside it. If, for example, the movie clip is larger than the stage, then we will only be able 
	 * 		to see part of it. The visible area rectangle represents exactly that. In practical use, sometimes the movie clip's (Starling's) stage is a different aspect 
	 * 		ratio than Flash's stage. In that case, cropping might occur. That's when the visible area rectangle becomes useful. The application can reference that 
	 * 		rectangle in order to determine where an asset can be placed to make sure it always stays visible.
	 * - Flash's stage dimensions are set at 768x1024 and will not change at run-time. They can be changed at author-time in order to simulate different device
	 * 		resolutions.
	 * - Starling's stage dimensions are set at 768x1024 and will not change at run-time. Assets have been created with this size in mind, which means changing these
	 * 		dimensions at author-time could cause problems. Therefore these dimensions will likely never change.
	 * - Starling's viewport dimensions start the same as Starling's stage dimensions, but the height will be set at run-time to Flash's stage dimensions, and the 
	 * 		width will be scaled accordingly. Depending on the difference between Flash's stage's aspect ratio and Starling's aspect ratio, cropping may occur on the 
	 * 		sides. Cropping is the preferred alternative to letter boxing, especially since Apple doesn't allow letter boxing on non-iPhone 5 devices.
	 * - The visible area dimensions are relative to Starling's stage dimensions, which will allow visual elements to be placed according to whether they can actually
	 * 		be seen.
	 * 
	 * @author Adam Testerman
	 */
	public class Startup extends Sprite
	{
		//CONSTANTS
		private const STARLING_STAGE_WIDTH:int = 			768;
		private const STARLING_STAGE_HEIGHT:int = 			1024;
		
		//STAGE OBJECTS
		public var txtLoading:TextField;
		public var txtEnterCustomAnswer:TextField;
		
		//OBJECTS
		private static var _visibleArea:Rectangle;
		private var _starling:Starling;
		private var _loadingBG:Bitmap;
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>Startup</code>.
		 */
		public function Startup() 
		{
			// These settings are recommended to avoid problems with touch handling
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(ResizeEvent.RESIZE, HandleStageResized);
			
			// set general properties
			SoundMixer.audioPlaybackMode = AudioPlaybackMode.AMBIENT;
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			stage.addEventListener(Event.ACTIVATE, HandleFocusGained);
			stage.addEventListener(Event.DEACTIVATE, HandleFocusLost);
			
			var iOS:Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;
			Starling.multitouchEnabled = iOS; // useful on mobile devices
			Starling.handleLostContext = !iOS; // not necessary on iOS. Saves a lot of memory!
			
			_starling = new Starling(WitchWay, stage);
			_starling.stage.stageWidth = STARLING_STAGE_WIDTH;
			_starling.stage.stageHeight = STARLING_STAGE_HEIGHT;
			_starling.antiAliasing = 16;
			_starling.enableErrorChecking = false;
			_starling.showStatsAt(HAlign.CENTER, VAlign.BOTTOM);	//DEBUG
			_starling.addEventListener(Event.CONTEXT3D_CREATE, RemovePreloader);
			
			//Make sure the input text is scaled appropriately to Flash's stage. It's sized to look good on a 768x1024 resolution at the moment
			Global.txtInputCustomAnswer = txtEnterCustomAnswer;
			Global.txtInputCustomAnswer.visible = false; //hide until screen is ready for it
			Global.txtInputCustomAnswer.scaleX = Global.txtInputCustomAnswer.scaleY = Global.txtInputCustomAnswerStartScale = stage.stageHeight / 1024;
			
			//Create and display loading image
			_loadingBG = new Bitmap(new BitmapData(stage.fullScreenWidth, stage.fullScreenHeight, false, 0));
			stage.addChild(_loadingBG);
			
			//Make sure the "loading..." text is scaled appropriately to Flash's stage. It's sized to look good on a 768x1024 resolution at the moment
			txtLoading.scaleX = txtLoading.scaleY = stage.stageHeight / 1024;
			txtLoading.x = (stage.fullScreenWidth - txtLoading.width) / 2;
			txtLoading.y = (stage.fullScreenHeight - txtLoading.height) / 2;
			txtLoading.cacheAsBitmap = true;
			stage.addChild(txtLoading);
		}
		
		
		//=================================
		//		MISC.
		//=================================
		
		/**
		 * Called when Starling is done loading. Removes the preloader and starts the Starling instance.
		 */
		private function RemovePreloader():void
		{
			_starling.removeEventListener(Event.CONTEXT3D_CREATE, RemovePreloader);
			
			_loadingBG.parent.removeChild(_loadingBG);
			_loadingBG.bitmapData.dispose();
			_loadingBG = null;
			txtLoading.parent.removeChild(txtLoading);
			txtLoading = null;
			
			_starling.start();
		}
		
		/**
		 * Called when the native stage resizes. Updates Starling's stage width, its viewport, and the visible area of the stage.
		 */
		private function HandleStageResized(event:Event):void 
		{
			if (_starling && _starling.stage)
			{
				_starling.viewPort = new Rectangle(0, 0, STARLING_STAGE_WIDTH, STARLING_STAGE_HEIGHT);
				
				var scale:Number = stage.stageHeight / _starling.viewPort.height;
				
				/*trace("BEFORE       flash stage: " + new Rectangle(stage.x, stage.y, stage.stageWidth, stage.stageHeight));
				trace("BEFORE    starling stage: " + new Rectangle(_starling.stage.x, _starling.stage.y, _starling.stage.stageWidth, _starling.stage.stageHeight));
				trace("BEFORE starling viewport: " + _starling.viewPort);
				trace("BEFORE      visible area: N/A");
				trace("BEFORE             scale: " + scale);
				trace("------");*/
				
				var viewPortRect:Rectangle = new Rectangle();
				viewPortRect.height = _starling.viewPort.height * scale;
				viewPortRect.width = _starling.viewPort.width * scale;
				viewPortRect.x = Math.round(stage.stageWidth / 2 - viewPortRect.width / 2);
				_starling.viewPort = viewPortRect;
				
				var lostWidth:int = (_starling.viewPort.width - stage.stageWidth) / scale;
				var lostHeight:int = (_starling.viewPort.height - stage.stageHeight) / scale;
				if (!_visibleArea) _visibleArea = new Rectangle();
				_visibleArea.height = _starling.stage.stageHeight - lostHeight;
				_visibleArea.width = _starling.stage.stageWidth - lostWidth;
				_visibleArea.x = _starling.stage.x + lostWidth / 2;
				_visibleArea.y = _starling.stage.y + lostHeight / 2;
				
				/*trace("AFTER        flash stage: " + new Rectangle(stage.x, stage.y, stage.stageWidth, stage.stageHeight));
				trace("AFTER     starling stage: " + new Rectangle(_starling.stage.x, _starling.stage.y, _starling.stage.stageWidth, _starling.stage.stageHeight));
				trace("AFTER  starling viewport: " + _starling.viewPort);
				trace("AFTER       visible area: " + _visibleArea);
				trace("------");
				trace("------");*/
				
				Global.scale = scale;
			}
		}
		
		/**
		 * Called when the stage gains focus (user comes back to the app from the desktop, for example). Returns the app's sound volume to its default levels.
		 */
		private function HandleFocusGained(event:Event):void 
		{
			trace("Startup.HandleFocusGained(): Returning volume to normal.");
			
			SoundEngine.getInstance().setVolume(1);
		}
		
		/**
		 * Called when the stage loses focus (user presses the home button, for example). Sets the app's sound volume to 0.
		 */
		private function HandleFocusLost(event:Event):void 
		{
			trace("Startup.HandleFocusGained(): Setting volume to 0.");
			
			SoundEngine.getInstance().setVolume(0);
		}
		
		
		//=================================
		//		ACCESSORS
		//=================================
		
		public static function get visibleArea():Rectangle 
		{
			return _visibleArea;
		}
	}
}