package com.jestermen.witchWay.ui.screens 
{
	import com.jestermen.managers.ObserverManager;
	import com.jestermen.witchWay.events.WitchWayEvent;
	import com.jestermen.witchWay.managers.AssetManager;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	
	/**
	 * Main menu screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenMainMenu extends Screen 
	{
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenMainMenu</code>.
		 */
		public function ScreenMainMenu():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var signAndTitle:Image = new Image(AssetManager.signMainMenu);
			signAndTitle.x = stage.stageWidth / 2 - signAndTitle.width / 2;
			signAndTitle.y = stage.stageHeight - signAndTitle.height;
			signAndTitle.touchable = false;
			addChild(signAndTitle);
			
			var btnPlay:Button = new Button(AssetManager.solidGray);	//use any texture; doesn't matter because it's going to have a different width/height and alpha 0
			btnPlay.width = 450;
			btnPlay.height = 120;
			btnPlay.x = stage.stageWidth / 2 - btnPlay.width / 2;
			btnPlay.y = stage.stageHeight - 594;	//430
			btnPlay.alpha = 0;
			btnPlay.addEventListener(Event.TRIGGERED, HandleTriggeredPlay);
			addChild(btnPlay);
			
			var btnHowTo:Button = new Button(AssetManager.solidGray);
			btnHowTo.width = 315;
			btnHowTo.height = 120;
			btnHowTo.x = stage.stageWidth / 2 - btnHowTo.width / 2;
			btnHowTo.y = stage.stageHeight - 454;	//570
			btnHowTo.alpha = 0;
			btnHowTo.addEventListener(Event.TRIGGERED, HandleTriggeredHowTo);
			addChild(btnHowTo);
			
			var btnOptions:Button = new Button(AssetManager.solidGray);
			btnOptions.width = 365;
			btnOptions.height = 120;
			btnOptions.x = stage.stageWidth / 2 - btnOptions.width / 2;
			btnOptions.y = stage.stageHeight - 314;	//710
			btnOptions.alpha = 0;
			btnOptions.addEventListener(Event.TRIGGERED, HandleTriggeredOptions);
			addChild(btnOptions);
			
			var btnAbout:Button = new Button(AssetManager.solidGray);
			btnAbout.width = 360;
			btnAbout.height = 120;
			btnAbout.x = stage.stageWidth / 2 - btnAbout.width / 2;
			btnAbout.y = stage.stageHeight - 144;	//880
			btnAbout.alpha = 0;
			btnAbout.addEventListener(Event.TRIGGERED, HandleTriggeredAbout);
			addChild(btnAbout);
		}
		
		
		//=================================
		//		BUTTON LISTENERS
		//=================================
		
		/**
		 * Called when the user triggers the play button on this screen. Sends out a notification so the application can determine what to do next.
		 */
		protected function HandleTriggeredPlay():void 
		{
			ObserverManager.notify(WitchWayEvent.USER_TRIGGERED_MAIN_PLAY);
		}
		
		/**
		 * Called when the user triggers the how to button on this screen. Sends out a notification so the application can determine what to do next.
		 */
		protected function HandleTriggeredHowTo():void 
		{
			ObserverManager.notify(WitchWayEvent.USER_TRIGGERED_MAIN_HOW_TO);
		}
		
		/**
		 * Called when the user triggers the options button on this screen. Sends out a notification so the application can determine what to do next.
		 */
		protected function HandleTriggeredOptions():void 
		{
			ObserverManager.notify(WitchWayEvent.USER_TRIGGERED_MAIN_OPTIONS);
		}
		
		/**
		 * Called when the user triggers the about button on this screen. Sends out a notification so the application can determine what to do next.
		 */
		protected function HandleTriggeredAbout():void 
		{
			ObserverManager.notify(WitchWayEvent.USER_TRIGGERED_MAIN_ABOUT);
		}
	}
}