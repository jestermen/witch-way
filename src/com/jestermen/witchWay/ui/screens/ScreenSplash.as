package com.jestermen.witchWay.ui.screens 
{
	import caurina.transitions.Tweener;
	import com.jestermen.utils.SoundEngine;
	import com.jestermen.witchWay.Config;
	import com.jestermen.witchWay.managers.AssetManager;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * Splash screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenSplash extends Sprite 
	{
		//PRIMITIVES
		private var _splashTimer:int;
		
		//OBJECTS
		private var _jestermenText:Image;
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenSplash</code>.
		 */
		public function ScreenSplash():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			addChild(new Quad(stage.stageWidth, stage.stageHeight, 0));	//pure black bg
			
			_jestermenText = new Image(AssetManager.jestermenText);
			_jestermenText.x = stage.stageWidth / 2 - _jestermenText.width / 2;
			_jestermenText.y = 200;
			addChild(_jestermenText);
			
			addEventListener(Event.ENTER_FRAME, SplashHideTimer);
		}
		
		
		/**
		 * Called every frame while the splash screen is up. Counts down until the splash screen can be removed.
		 */
		private function SplashHideTimer(event:Event):void 
		{
			_splashTimer++;
			
			//Start fading out text
			if (_splashTimer == Config.SPLASH_TIMER_TEXT)
			{
				Tweener.addTween(_jestermenText, { alpha:0, time:1, transition:"linear" } );
			}
			//Start fading out background and play intro sound
			else if (_splashTimer == Config.SPLASH_TIMER_ALL)
			{
				Tweener.addTween(this, { alpha:0, time:1, transition:"linear" } );
				
				SoundEngine.getInstance().playSound(Config.SFX_WHISTLING_CACKLE);
			}
			//Remove splash screen altogether
			else if (_splashTimer == Config.SPLASH_TIMER_END)
			{
				removeEventListener(Event.ENTER_FRAME, SplashHideTimer);
				
				removeFromParent(true);
			}
		}
	}
}