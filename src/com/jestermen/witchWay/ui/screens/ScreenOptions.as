package com.jestermen.witchWay.ui.screens 
{
	import com.jestermen.managers.ObserverManager;
	import com.jestermen.utils.SoundEngine;
	import com.jestermen.witchWay.events.WitchWayEvent;
	import com.jestermen.witchWay.managers.AssetManager;
	import com.jestermen.witchWay.Startup;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * Options screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenOptions extends Screen 
	{
		//PRIMITIVES
		private var _isSoundOff:Boolean;
		
		//OBJECTS
		private var _se:SoundEngine = SoundEngine.getInstance();
		private var _iconSoundOff:Image;
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenOptions</code>.
		 */
		public function ScreenOptions():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			x = _startX = stage.stageWidth;
			
			_isSoundOff = _se.getVolume() == 0;
			
			var sign:Image = new Image(AssetManager.signOptions);
			sign.x = stage.stageWidth / 2 - sign.width / 2;
			sign.y = stage.stageHeight - sign.height;
			sign.touchable = false;
			addChild(sign);
			
			var btnBack:Button = new Button(AssetManager.signBack);
			btnBack.x = Startup.visibleArea.left;
			btnBack.y = stage.stageHeight - btnBack.height;
			btnBack.addEventListener(Event.TRIGGERED, HandleTriggeredBack);
			addChild(btnBack);
			
			var btnCustomAnswers:Button = new Button(AssetManager.signCustomAnswers);
			btnCustomAnswers.x = stage.stageWidth / 2 - btnCustomAnswers.width / 2;
			btnCustomAnswers.y = stage.stageHeight - 394;	//630
			btnCustomAnswers.addEventListener(Event.TRIGGERED, HandleTriggeredCustomAnswersButton);
			addChild(btnCustomAnswers);
			
			var btnToggleSound:Button = new Button(AssetManager.signToggleSound);
			btnToggleSound.x = Startup.visibleArea.right - btnToggleSound.width;
			btnToggleSound.y = stage.stageHeight - btnToggleSound.height;
			btnToggleSound.addEventListener(Event.TRIGGERED, ToggleSound);
			addChild(btnToggleSound);
			
			_iconSoundOff = new Image(AssetManager.iconSoundOff);
			_iconSoundOff.x = btnToggleSound.width / 2 - _iconSoundOff.width / 2;
			_iconSoundOff.y = 6.5;
			_iconSoundOff.visible = _isSoundOff;
			Sprite(btnToggleSound.getChildAt(0)).addChild(_iconSoundOff);
		}
		
		
		//=================================
		//		BUTTON LISTENERS
		//=================================
		
		/**
		 * Called when the user triggers the custom answers button. Sends out a notification so the application can determine what to do next.
		 */
		private function HandleTriggeredCustomAnswersButton():void 
		{
			ObserverManager.notify(WitchWayEvent.USER_TRIGGERED_OPTIONS_CUSTOM_ANSWERS, this);
		}
		
		/**
		 * Called when the user triggers the toggle sound button. Toggles the entire application's sound.
		 */
		private function ToggleSound(event:Event):void 
		{
			//_se.mute();		//toggles automatically (NOT NEEDED? BROKEN?)
			
			_isSoundOff = !_isSoundOff;
			
			if (_isSoundOff)
			{
				_se.setVolume(0);
				
				_iconSoundOff.visible = true;
			}
			else
			{
				_se.setVolume(.5);
				
				_iconSoundOff.visible = false;
			}
		}
	}
}