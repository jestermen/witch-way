package com.jestermen.witchWay.ui.screens 
{
	import caurina.transitions.Tweener;
	import com.jestermen.utils.SoundEngine;
	import com.jestermen.witchWay.Global;
	import com.jestermen.witchWay.managers.AssetManager;
	import com.jestermen.witchWay.Startup;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import flash.utils.setTimeout;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.HAlign;
	
	/**
	 * Custom answers screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenCustomAnswers extends Screen 
	{
		//CONSTANTS
		private const SCROLL_CONTENT_START_Y:int = 							64;
		private const INPUT_CUSTOM_ANSWER_DEFAULT_TEXT:String =				"Add an answer";
		private const MAX_VISIBLE_CUSTOM_ANSWER_TILES:int =					6;
		
		//OBJECTS
		private var _txtInputCustomAnswer:flash.text.TextField;
		private var _check:Image;
		private var _checkBox:Button;
		private var _scrollTop:Image;
		private var _scrollBody:Sprite;
		private var _scrollInputTextBorder:Image;
		private var _scrollContentContainer:Sprite;
		private var _scrollBottom:Image;
		
		//LISTS
		private var _customAnswerTiles:Array = [];
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenCustomAnswers</code>.
		 */
		public function ScreenCustomAnswers():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			x = _startX = stage.stageWidth;
			
			_scrollTop = new Image(AssetManager.scrollTop);
			_scrollTop.x = stage.stageWidth / 2 - _scrollTop.width / 2;
			_scrollTop.y = stage.stageHeight / 2 - _scrollTop.height;
			addChild(_scrollTop);
			
			_scrollBody = new Sprite();
			_scrollBody.addChild(new Image(AssetManager.scrollBody));
			_scrollBody.x = stage.stageWidth / 2 - _scrollBody.width / 2;
			_scrollBody.y = stage.stageHeight / 2;
			_scrollBody.clipRect = new Rectangle(0, 0, _scrollBody.width, _scrollBody.height);
			_scrollBody.scaleY = 0;
			_scrollBody.addEventListener(TouchEvent.TOUCH, HandleTouchedScrollBody);
			addChild(_scrollBody);
			
			_scrollContentContainer = new Sprite();
			_scrollContentContainer.y = SCROLL_CONTENT_START_Y;
			_scrollBody.addChild(_scrollContentContainer);
			
			var scrollHeaderText:Image = new Image(AssetManager.scrollHeaderText);
			scrollHeaderText.x = 54;
			_scrollContentContainer.addChild(scrollHeaderText);
			
			var scrollCustomOnlyText:Image = new Image(AssetManager.scrollCustomOnlyText);
			scrollCustomOnlyText.x = 54;
			scrollCustomOnlyText.y = 100;
			_scrollContentContainer.addChild(scrollCustomOnlyText);
			
			_scrollInputTextBorder = new Image(AssetManager.scrollInputTextBorder);
			_scrollInputTextBorder.x = 54;
			_scrollInputTextBorder.y = 200;
			_scrollContentContainer.addChild(_scrollInputTextBorder);
			
			_txtInputCustomAnswer = Global.txtInputCustomAnswer;	//found on the native stage, set in Startup
			_txtInputCustomAnswer.text = INPUT_CUSTOM_ANSWER_DEFAULT_TEXT;
			_txtInputCustomAnswer.addEventListener(FocusEvent.FOCUS_IN, HandleFocusedInCustomAnswer);
			_txtInputCustomAnswer.addEventListener(KeyboardEvent.KEY_DOWN, HandleKeyedDownCustomAnswer);
			
			_check = new Image(AssetManager.check);
			_check.x = 8;
			_check.y = -12;
			_check.touchable = false;
			
			_checkBox = new Button(AssetManager.checkBox);
			_checkBox.x = 494;
			_checkBox.y = 120;
			_checkBox.addEventListener(Event.TRIGGERED, HandleTriggeredUseCustomAnswersOnly);
			_scrollContentContainer.addChild(_checkBox);
			
			//Create a tile for each custom answer that the user has saved
			for (var i:int = 0; i < Global.witchAnswers.answer.length(); i++) 
			{
				if (Global.witchAnswers.answer[i].@isDefault == "false")
				{
					CreateCustomAnswerTile(Global.witchAnswers.answer[i].@text);
				}
			}
			
			_scrollBottom = new Image(AssetManager.scrollBottom);
			_scrollBottom.x = stage.stageWidth / 2 - _scrollBottom.width / 2;
			_scrollBottom.y = stage.stageHeight / 2;
			addChild(_scrollBottom);
			
			var btnBack:Button = new Button(AssetManager.signBack);
			btnBack.x = Startup.visibleArea.left;
			btnBack.y = stage.stageHeight - btnBack.height;
			btnBack.addEventListener(Event.TRIGGERED, HandleTriggeredBack);
			addChild(btnBack);
			
			var btnAddCustomAnswer:Button = new Button(AssetManager.customAnswerAdd);
			btnAddCustomAnswer.x = _checkBox.x;
			btnAddCustomAnswer.y = 212;
			btnAddCustomAnswer.addEventListener(Event.TRIGGERED, HandleTriggeredAddCustomAnswer);
			_scrollContentContainer.addChild(btnAddCustomAnswer);
			
			updateDisplay();
			
			Tweener.addTween(this, { alpha:1, time:1, onComplete:UnfurlScroll } );
		}
		
		
		/**
		 * Prepares this object for garbage collection.
		 */
		public override function destroy():void 
		{
			_txtInputCustomAnswer.visible = false;
			
			super.destroy();
		}
		
		
		//=================================
		//		CUSTOM ANSWER INPUT
		//=================================
		
		/**
		 * Called when the input custom answer text field gains focus. If the text field contains the default instructions text, the text field will be cleared. 
		 * Otherwise, all of the text in the text field will be selected.
		 */
		private function HandleFocusedInCustomAnswer(event:FocusEvent):void 
		{
			if (_txtInputCustomAnswer.text == INPUT_CUSTOM_ANSWER_DEFAULT_TEXT) _txtInputCustomAnswer.text = "";
			else setTimeout(_txtInputCustomAnswer.setSelection, 50, 0, _txtInputCustomAnswer.text.length);	//need a delay; won't work otherwise
		}
		
		/**
		 * Called when a keyboard key is pressed while the custom answer text field has focus. If the Enter key was pressed, prevent it from line breaking and instead
		 * trigger the answer submission.
		 */
		private function HandleKeyedDownCustomAnswer(event:KeyboardEvent):void 
		{
			if (event.keyCode == Keyboard.ENTER)
			{
				event.preventDefault();
				
				HandleTriggeredAddCustomAnswer();
			}
		}
		
		
		//=================================
		//		CUSTOM ANSWER TILES
		//=================================
		
		/**
		 * Creates a visible tile that holds one of the user's custom answers and places the tile onto the screen.
		 */
		private function CreateCustomAnswerTile(tileText:String):void 
		{
			//Create the pieces of the tile
			var txtTileText:starling.text.TextField = new starling.text.TextField(400, 70, tileText, "Zombie Guts", 53, 0x242013);
			txtTileText.touchable = false;
			txtTileText.autoScale = true;
			txtTileText.hAlign = HAlign.LEFT;
			
			var btnTileX:Button = new Button(AssetManager.customAnswerRemove);
			btnTileX.name = "btnTileX";
			btnTileX.x = 442;
			btnTileX.y = txtTileText.height / 2 - btnTileX.height / 2;
			btnTileX.addEventListener(Event.TRIGGERED, HandleTriggeredRemoveCustomAnswer);
			
			//Create the tile itself and add the pieces to it
			var newTile:Sprite = new Sprite();
			newTile.name = tileText;
			newTile.addChild(txtTileText);
			newTile.addChild(btnTileX);
			
			_customAnswerTiles.push(newTile);
			
			PopulateTileMenu();
		}
		
		/**
		 * Called when the user triggers the add custom answer button. Adds the text found in the custom answer text field to the list of the user's custom answers.
		 */
		private function HandleTriggeredAddCustomAnswer():void 
		{
			//Disallow answer under certain conditions
			if (_txtInputCustomAnswer.text == "" || _txtInputCustomAnswer.text == INPUT_CUSTOM_ANSWER_DEFAULT_TEXT)
			{
				Starling.current.nativeStage.focus = _txtInputCustomAnswer;
				
				return;
			}
			
			//Truncate the custom answer if it's too long
			if (_txtInputCustomAnswer.text.length > 50)
			{
				//trace("text too long; shortening. old text: " + txtEnterCustomAnswer.text);
				_txtInputCustomAnswer.text = _txtInputCustomAnswer.text.substring(0, 49);
				//trace("new text: " + txtEnterCustomAnswer.text);
			}
			
			//Add the custom answer to the XML and save it
			var childToAppend:XML = XML(<answer text="" isDefault="false"/>);
			childToAppend.@text = _txtInputCustomAnswer.text;
			
			Global.witchAnswers.appendChild(childToAppend);
			
			//trace("appended: " + Global.witchAnswers);
			
			//Add the custom answer to the visible list of custom answers
			CreateCustomAnswerTile(_txtInputCustomAnswer.text);
			
			//Reset the text in the text field
			_txtInputCustomAnswer.text = "";
			Starling.current.nativeStage.focus = _txtInputCustomAnswer;
		}
		
		/**
		 * Called when the user triggers the remove custom answer button. Removes the corresponding custom answer and its tile. If the last custom answer was removed, 
		 * unchecks the use custom answers only box.
		 */
		private function HandleTriggeredRemoveCustomAnswer(event:Event):void 
		{
			var tileToRemove:Sprite = Sprite(Button(event.currentTarget).parent);
			
			//Remove the custom answer from the XML and save it
			for (var i:int = 0; i < Global.witchAnswers.answer.length(); i++) 
			{
				if (Global.witchAnswers.answer[i].@text == tileToRemove.name)
				{
					delete Global.witchAnswers.answer[i];
					
					break;
				}
			}
			
			//Remove the tile from the list of tiles, then repopulate the tile menu
			tileToRemove.removeFromParent(true);
			_customAnswerTiles.splice(_customAnswerTiles.indexOf(tileToRemove), 1);
			
			PopulateTileMenu();
			
			if (_customAnswerTiles.length == 0) Global.isUsingCustomAnswersOnly = false;
		}
		
		/**
		 * Displays the custom answer tiles, ensuring that their backgrounds alternate. The tiles will be listed in order of newest to oldest.
		 */
		private function PopulateTileMenu():void 
		{
			var thisTile:Sprite;
			for (var i:int = _customAnswerTiles.length - 1; i >= 0; i--)
			{
				thisTile = _customAnswerTiles[i];
				
				thisTile.x = 57;
				thisTile.y = (351 - thisTile.height) + thisTile.height * (_customAnswerTiles.length - 1 - i);
				
				_scrollContentContainer.addChild(thisTile);
			}
		}
		
		
		//=================================
		//		MISC.
		//=================================
		
		/**
		 * Ensures that the custom answers only check box and the input custom answer text field are in the proper states.
		 */
		public function updateDisplay():void 
		{
			Global.isUsingCustomAnswersOnly ? Sprite(_checkBox.getChildAt(0)).addChild(_check) : _check.removeFromParent();
			
			//Input text field
			var scrollInputBorderGlobalPosition:Point = _scrollInputTextBorder.localToGlobal(new Point(0, 0));
			_txtInputCustomAnswer.x = scrollInputBorderGlobalPosition.x * Global.scale - Startup.visibleArea.left * Global.scale;
			_txtInputCustomAnswer.y = Startup.visibleArea.top + scrollInputBorderGlobalPosition.y * Global.scale;
			_txtInputCustomAnswer.scaleY = Global.txtInputCustomAnswerStartScale * _scrollBody.scaleY;
			
			//Only show input text field if it's not off scroll
			if (_txtInputCustomAnswer.y >= _scrollBody.y && _txtInputCustomAnswer.y <= _scrollBottom.y)
			{
				_txtInputCustomAnswer.visible = true;
			}
			else 
			{
				_txtInputCustomAnswer.visible = false;
			}
		}
		
		/**
		 * Called when the user touches the scroll body. Allows the user to scroll the scroll (that's right) if it has enough tiles.
		 */
		private function HandleTouchedScrollBody(event:TouchEvent):void 
		{
			if (_scrollContentContainer.height + SCROLL_CONTENT_START_Y > _scrollBody.clipRect.height)
			{
				var touch:Touch = event.getTouch(_scrollBody);
				if (touch)
				{
					//Mouse move: Scroll container
					if (touch.phase == TouchPhase.MOVED)
					{
						//Move container and input text field
						_scrollContentContainer.y += touch.getMovement(this).y;
						
						if (_scrollContentContainer.y > SCROLL_CONTENT_START_Y)
						{
							_scrollContentContainer.y = SCROLL_CONTENT_START_Y;
						}
						else if (_scrollContentContainer.y < _scrollBody.clipRect.bottom - _scrollContentContainer.height) 
						{
							_scrollContentContainer.y = _scrollBody.clipRect.bottom - _scrollContentContainer.height;
						}
						
						updateDisplay();
					}
				}
			}
		}
		
		/**
		 * Called when the user triggers the use custom answers only check box. Toggles the check box off if it is on, or on if it is off. It can only be toggled on
		 * if there are custom answers available.
		 */
		private function HandleTriggeredUseCustomAnswersOnly():void 
		{
			if (Global.isUsingCustomAnswersOnly) Global.isUsingCustomAnswersOnly = false;
			else if (_customAnswerTiles.length > 0) Global.isUsingCustomAnswersOnly = true;
			
			updateDisplay();
		}
		
		/**
		 * Animates the scroll, unfurling it and playing an appropriate sound effect.
		 */
		private function UnfurlScroll():void 
		{
			Tweener.addTween(_scrollBody, { scaleY:1, time:1, onUpdate:CenterScroll } );
			
			SoundEngine.getInstance().playSound("Scroll");
		}
		
		/**
		 * Called while the scroll is unfurling. Ensures that the scroll is centered vertically on the screen.
		 */
		private function CenterScroll():void 
		{
			_scrollBody.y = stage.stageHeight / 2 - _scrollBody.height / 2;
			_scrollTop.y = _scrollBody.y - _scrollTop.height + 10;
			_scrollBottom.y = _scrollBody.y + _scrollBody.height - 10;
			
			updateDisplay();
		}
	}
}