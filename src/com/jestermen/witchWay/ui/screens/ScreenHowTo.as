package com.jestermen.witchWay.ui.screens 
{
	import com.jestermen.witchWay.managers.AssetManager;
	import com.jestermen.witchWay.Startup;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	
	/**
	 * How To screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenHowTo extends Screen 
	{
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenHowTo</code>.
		 */
		public function ScreenHowTo():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			x = _startX = -stage.stageWidth;
			
			var sign:Image = new Image(AssetManager.signHowTo);
			sign.x = stage.stageWidth / 2 - sign.width / 2;
			sign.y = stage.stageHeight - sign.height;
			sign.touchable = false;
			addChild(sign);
			
			var btnBack:Button = new Button(AssetManager.signBack);
			btnBack.x = Startup.visibleArea.right;	//not subtracting its width here because its scaleX is -1
			btnBack.y = stage.stageHeight - btnBack.height;
			btnBack.scaleX = -1;
			btnBack.addEventListener(Event.TRIGGERED, HandleTriggeredBack);
			addChild(btnBack);
		}
	}
}