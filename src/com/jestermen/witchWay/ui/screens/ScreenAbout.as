package com.jestermen.witchWay.ui.screens 
{
	import com.jestermen.witchWay.managers.AssetManager;
	import com.jestermen.witchWay.Startup;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * About screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenAbout extends Screen 
	{
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenAbout</code>.
		 */
		public function ScreenAbout():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			x = _startX = -stage.stageWidth;
			
			var signs:Image = new Image(AssetManager.signAbout);
			signs.x = stage.stageWidth / 2 - signs.width / 2;
			signs.y = stage.stageHeight - signs.height;
			signs.touchable = false;
			addChild(signs);
			
			var btnVisitCreepspace:Button = new Button(AssetManager.solidGray);	//use any texture; doesn't matter because it's going to have a different width/height and alpha 0
			btnVisitCreepspace.width = 480;
			btnVisitCreepspace.height = 265;
			btnVisitCreepspace.x = stage.stageWidth / 2 - btnVisitCreepspace.width / 2;
			btnVisitCreepspace.y = stage.stageHeight - 661;	//363
			btnVisitCreepspace.alpha = 0;
			btnVisitCreepspace.addEventListener(Event.TRIGGERED, HandleTriggeredVisitCreepspace);
			addChild(btnVisitCreepspace);
			
			var btnVisitJestermen:Button = new Button(AssetManager.solidGray);	//use any texture; doesn't matter because it's going to have a different width/height and alpha 0
			btnVisitJestermen.width = 480;
			btnVisitJestermen.height = 265;
			btnVisitJestermen.x = stage.stageWidth / 2 - btnVisitJestermen.width / 2;
			btnVisitJestermen.y = stage.stageHeight - 399;	//625
			btnVisitJestermen.alpha = 0;
			btnVisitJestermen.addEventListener(Event.TRIGGERED, HandleTriggeredVisitJestermen);
			addChild(btnVisitJestermen);
			
			var btnBack:Button = new Button(AssetManager.signBack);
			btnBack.x = Startup.visibleArea.right;	//not subtracting its width here because its scaleX is -1
			btnBack.y = stage.stageHeight - btnBack.height;
			btnBack.scaleX = -1;
			btnBack.addEventListener(Event.TRIGGERED, HandleTriggeredBack);
			addChild(btnBack);
			
			var starlingCredit:Image = new Image(Texture.fromBitmapData(new StarlingCredit(), false));
			starlingCredit.x = Startup.visibleArea.left;
			starlingCredit.y = stage.stageHeight - starlingCredit.height;
			addChild(starlingCredit);
		}
		
		
		//=================================
		//		BUTTON LISTENERS
		//=================================
		
		/**
		 * Called when the user triggers the visit Creepspace button. Loads the website in an external browser.
		 */
		private function HandleTriggeredVisitCreepspace():void 
		{
			navigateToURL(new URLRequest("http://creepspace.com"));
		}
		
		/**
		 * Called when the user triggers the visit Jestermen button. Loads the website in an external browser.
		 */
		private function HandleTriggeredVisitJestermen():void 
		{
			navigateToURL(new URLRequest("http://jestermen.com"));
		}
	}
}