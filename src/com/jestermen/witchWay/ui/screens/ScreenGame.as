package com.jestermen.witchWay.ui.screens 
{
	import caurina.transitions.Tweener;
	import com.jestermen.utils.SoundEngine;
	import com.jestermen.witchWay.Config;
	import com.jestermen.witchWay.Global;
	import com.jestermen.witchWay.managers.AssetManager;
	import com.jestermen.witchWay.Startup;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.extensions.ColorArgb;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.VAlign;
	
	/**
	 * Game screen for "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class ScreenGame extends Screen 
	{
		//OBJECTS
		private var _witch:MovieClip;
		private var _witchOrbRoiling:MovieClip;
		private var _witchOrbFlashing:MovieClip;
		private var _witchOrbExploding:MovieClip;
		private var _txtAnswer:TextField;
		private var _btnAsk:Button;
		private var _btnBack:Button;
		private var _btnReset:Button;
		private var _se:SoundEngine = SoundEngine.getInstance();
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>ScreenGame</code>.
		 */
		public function ScreenGame():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		/**
		 * Initializes this object. This method should only be called after the object has been added to the stage.
		 */
		private function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			x = _startX = stage.stageWidth;
			
			//Set up the particle system
			var ps:PDParticleSystem = AssetManager.particleSystem;
			addChild(ps);
			//ps.emitterType = 1;		//radial
			ps.emitterX = stage.stageWidth / 2;
			ps.emitterY = stage.stageHeight / 2;
			ps.maxNumParticles = 850;
			ps.lifespan = 2.7;
			ps.lifespanVariance = 0;
			ps.startSize = 43.8;
			ps.startSizeVariance = 0;
			ps.endSize = 58.1;
			ps.endSizeVariance = 0;
			ps.emitAngle = 0;
			ps.emitAngleVariance = 360;
			ps.startRotation = 0;
			ps.startRotationVariance = 0;
			ps.endRotation = 145;
			ps.endRotationVariance = 0;
			ps.maxRadius = 80;
			ps.maxRadiusVariance = 20.4;
			ps.minRadius = 0;
			ps.rotatePerSecond = 4;
			ps.rotatePerSecondVariance = 0;
			ps.startColor = ColorArgb.fromArgb(0x0600FF00);
			ps.startColorVariance = ColorArgb.fromArgb(0x10000000);
			ps.endColor = ColorArgb.fromArgb(0x004C4C4C);
			ps.endColorVariance = ColorArgb.fromArgb(0x00000000);
			ps.start();
			
			//Create and set up the witch
			_witch = new MovieClip(AssetManager.witchStir, Config.WITCH_ANIM_FPS);
			_witch.x = stage.stageWidth / 2 - _witch.width / 2;
			_witch.y = stage.stageHeight - _witch.height;
			_witch.loop = false;
			_witch.stop();
			//addChild(_witch);
			Starling.juggler.add(_witch);
			
			addChild(AssetManager.skeleton);
			AssetManager.skeleton.x = stage.stageWidth / 2 - AssetManager.skeleton.width / 2;
			AssetManager.skeleton.y = stage.stageHeight - AssetManager.skeleton.height;
			
			//--Roiling orb
			_witchOrbRoiling = new MovieClip(AssetManager.witchOrbRoil, Config.WITCH_ANIM_FPS);
			_witchOrbRoiling.stop();
			_witchOrbRoiling.pivotX = _witchOrbRoiling.width / 2;
			_witchOrbRoiling.pivotY = _witchOrbRoiling.height / 2;
			Starling.juggler.add(_witchOrbRoiling);
			
			//--Green screen flash
			var witchOrbFlashTextures:Vector.<Texture> = new Vector.<Texture>()
			witchOrbFlashTextures.push(Texture.fromColor(stage.stageWidth, stage.stageHeight, 0xFF7EFF40));
			_witchOrbFlashing = new MovieClip(witchOrbFlashTextures, Config.WITCH_ANIM_FPS);
			_witchOrbFlashing.stop();
			Starling.juggler.add(_witchOrbFlashing);
			
			//--Exploding orb
			_witchOrbExploding = new MovieClip(AssetManager.witchOrbExplode, Config.WITCH_ANIM_FPS);
			_witchOrbExploding.stop();
			_witchOrbExploding.pivotX = _witchOrbExploding.width / 2;
			_witchOrbExploding.pivotY = _witchOrbExploding.height / 2;
			Starling.juggler.add(_witchOrbExploding);
			
			//Create and hide the text field that will hold the witch's answers
			_txtAnswer = new TextField(460, 400, "NO DOUBT ABOUT IT", "Zombie Guts", 72, 0x01C500);
			_txtAnswer.x = stage.stageWidth / 2 -_txtAnswer.width / 2;
			_txtAnswer.y = stage.stageHeight - 935;	//89
			_txtAnswer.autoScale = true;
			_txtAnswer.alpha = 0;
			//_txtAnswer.border = true;	//DEBUG
			_txtAnswer.vAlign = VAlign.CENTER;
			addChild(_txtAnswer);
			
			//Create and set up the buttons for this screen
			_btnAsk = new Button(AssetManager.signAsk);
			_btnAsk.x = Startup.visibleArea.right - _btnAsk.width;
			_btnAsk.y = stage.stageHeight - _btnAsk.height;
			_btnAsk.addEventListener(Event.TRIGGERED, HandleTriggeredAsk);
			addChild(_btnAsk);
			
			_btnBack = new Button(AssetManager.signBack);
			_btnBack.x = Startup.visibleArea.left;
			_btnBack.y = stage.stageHeight - _btnBack.height;
			_btnBack.addEventListener(Event.TRIGGERED, HandleTriggeredBack);
			addChild(_btnBack);
			
			_btnReset = new Button(AssetManager.signReset);
			_btnReset.x = _btnAsk.x;
			_btnReset.y = stage.stageHeight + 100;
			_btnReset.addEventListener(Event.TRIGGERED, HandleTriggeredReset);
			addChild(_btnReset);
		}
		
		
		//=================================
		//		BUTTON LISTENERS
		//=================================
		
		/**
		 * Called when the user triggers the back button. Returns the application to the main menu.
		 */
		protected override function HandleTriggeredBack():void 
		{
			_se.stopAllSounds();
			
			HandleTriggeredReset();			//manually trigger reset so the ask button will be immediately available next time we go back to the game screen
			
			super.HandleTriggeredBack();	//call base function to finish the method
		}
		
		/**
		 * Called when the user clicks the ask button. Begins animating the witch and the steam.
		 */
		private function HandleTriggeredAsk():void 
		{
			//Start animations
			PlayWitchSequence();
			
			//Play sounds
			_se.stopSound(Config.SFX_WHISTLING_CACKLE);	//in case user gets here and presses ask quickly
			_se.playSound(Config.SFX_CACKLE);
			
			//Hide ask button
			_btnAsk.removeEventListener(Event.TRIGGERED, HandleTriggeredAsk);
			Tweener.addTween(_btnAsk, { y:stage.stageHeight + 100, time:Config.SCREEN_TRANSITION_TIME } );
			
			//Show reset button
			_btnReset.addEventListener(Event.TRIGGERED, HandleTriggeredReset);
			Tweener.addTween(_btnReset, { y:stage.stageHeight - _btnReset.height, time:Config.SCREEN_TRANSITION_TIME } );
		}
		
		/**
		 * Called when the user triggers the reset button. Allows the user to ask another question and receive another answer.
		 */
		private function HandleTriggeredReset():void 
		{
			//Stop animations
			HideWitchAnswer();
			StopAnimations();
			
			//Stop sounds
			_se.stopSound(Config.SFX_CACKLE);
			
			//Hide reset button
			_btnReset.removeEventListener(Event.TRIGGERED, HandleTriggeredReset);
			Tweener.addTween(_btnReset, { y:stage.stageHeight + 100, time:Config.SCREEN_TRANSITION_TIME } );
			
			//Show ask button
			_btnAsk.addEventListener(Event.TRIGGERED, HandleTriggeredAsk);
			Tweener.addTween(_btnAsk, { y:stage.stageHeight - _btnAsk.height, time:Config.SCREEN_TRANSITION_TIME } );
		}
		
		
		//=================================
		//		SHOW/HIDE ANSWERS
		//=================================
		
		/**
		 * Chooses a random answer for the witch and begins transitioning it onto the screen.
		 */
		private function ShowWitchAnswer():void 
		{
			var randAnswerIndex:int;
			
			//Search for a random answer, using only custom answers if necessary
			do
			{
				randAnswerIndex = Math.random() * Global.witchAnswers.answer.length();
				
				//If we're not using only custom answers, OR if we are using only custom answers and we just found a custom answer, continue
				if (!Global.isUsingCustomAnswersOnly || (Global.isUsingCustomAnswersOnly && Global.witchAnswers.answer[randAnswerIndex].@isDefault == "false")) break;
			}
			while (true)
			
			var randAnswer:String = Global.witchAnswers.answer[randAnswerIndex].@text;
			
			_txtAnswer.text = randAnswer;
			Tweener.addTween(_txtAnswer, { alpha:1, time:Config.ANSWER_TRANSITION_TIME } );
		}
		
		/**
		 * Begins transitioning the witch's answer off the screen.
		 */
		private function HideWitchAnswer():void 
		{
			Tweener.addTween(_txtAnswer, { alpha:0, time:Config.ANSWER_TRANSITION_TIME } );
		}
		
		
		//=================================
		//		WITCH PLAYBACK
		//=================================
		
		/**
		 * Plays the witch stir animation.
		 */
		private function PlayWitchSequence():void 
		{
			_witch.stop();	//make sure she resets first
			_witch.play();
			_witch.addEventListener(Event.COMPLETE, PlayWitchOrbRoiling);
		}
		
		/**
		 * Plays the first part of the steam animation.
		 */
		private function PlayWitchOrbRoiling():void 
		{
			//Stop the previous animation
			_witch.stop();
			_witch.removeEventListener(Event.COMPLETE, PlayWitchOrbRoiling);
			
			//Start this animation
			_witchOrbRoiling.scaleX = _witchOrbRoiling.scaleY = .16;	//starts small and tweens up
			_witchOrbRoiling.x = -84;
			_witchOrbRoiling.y = 730;
			_witchOrbRoiling.play();
			_witchOrbRoiling.addEventListener(Event.COMPLETE, PlayWitchOrbFlashing);
			addChild(_witchOrbRoiling);
			
			//Tween this clip's scale and position
			Tweener.addTween(_witchOrbRoiling, { 	x:-186, 			//move halfway across the curve
													time:_witchOrbRoiling.totalTime / 2, 
													transition:"easeOutCubic" } );
			Tweener.addTween(_witchOrbRoiling, { 	x:0, 									//then move the rest of the way (can't find an equation that fits a single tween)
													time:_witchOrbRoiling.totalTime / 2, 
													transition:"easeInCubic", 
													delay:_witchOrbRoiling.totalTime / 2 } );
			Tweener.addTween(_witchOrbRoiling, {	y:290, 
													scaleX:1, 
													scaleY:1, 
													time:_witchOrbRoiling.totalTime, 
													transition:"linear" } );
			
			//Play sfx
			SoundEngine.getInstance().playSound("MagicExplosion");
		}
		
		/**
		 * Plays the middle part of the steam animation and reveals the witch's answer.
		 */
		private function PlayWitchOrbFlashing():void 
		{
			//Stop the previous animation
			_witchOrbRoiling.stop();
			_witchOrbRoiling.removeEventListener(Event.COMPLETE, PlayWitchOrbFlashing);
			_witchOrbRoiling.removeFromParent();
			
			//Start this animation
			_witchOrbFlashing.x = -_witchOrbFlashing.width / 2;
			_witchOrbFlashing.play();
			_witchOrbFlashing.addEventListener(Event.COMPLETE, PlayWitchOrbExploding);
			addChild(_witchOrbFlashing);
			
			ShowWitchAnswer();
		}
		
		/**
		 * Plays the last part of the steam animation.
		 */
		private function PlayWitchOrbExploding():void 
		{
			//Stop the previous animation
			_witchOrbFlashing.stop();
			_witchOrbFlashing.removeEventListener(Event.COMPLETE, PlayWitchOrbExploding);
			_witchOrbFlashing.removeFromParent();
			
			//Start this animation
			_witchOrbExploding.x = _witchOrbRoiling.x;
			_witchOrbExploding.y = _witchOrbRoiling.y;
			_witchOrbExploding.play();
			_witchOrbExploding.addEventListener(Event.COMPLETE, StopAnimations);
			addChild(_witchOrbExploding);
		}
		
		/**
		 * Removes the steam from the screen and resets both the steam and the witch animations to their default frames.
		 */
		private function StopAnimations():void 
		{
			_witch.stop();
			_witch.removeEventListener(Event.COMPLETE, PlayWitchOrbRoiling);
			
			if (_witchOrbRoiling)
			{
				_witchOrbRoiling.stop();
				_witchOrbRoiling.removeEventListener(Event.COMPLETE, PlayWitchOrbFlashing);
				_witchOrbRoiling.removeFromParent();
			}
			
			if (_witchOrbFlashing)
			{
				_witchOrbFlashing.stop();
				_witchOrbFlashing.removeEventListener(Event.COMPLETE, PlayWitchOrbExploding);
				_witchOrbFlashing.removeFromParent();
			}
			
			if (_witchOrbExploding)
			{
				_witchOrbExploding.stop();
				_witchOrbExploding.removeEventListener(Event.COMPLETE, PlayWitchOrbRoiling);
				_witchOrbExploding.removeFromParent();
			}
		}
	}
}