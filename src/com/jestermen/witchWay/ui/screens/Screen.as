package com.jestermen.witchWay.ui.screens 
{
	import caurina.transitions.Tweener;
	import com.jestermen.managers.ObserverManager;
	import com.jestermen.witchWay.events.WitchWayEvent;
	import starling.display.Sprite;
	
	/**
	 * This is an abstract base class for all of the game screens in "Witch Way" Starling version.
	 * 
	 * @author Adam Testerman
	 */
	public class Screen extends Sprite 
	{
		//PRIMITIVES
		protected var _startX:int;
		
		
		//=================================
		//		SETUP
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>Screen</code>.
		 */
		public function Screen():void 
		{
			
		}
		
		
		/**
		 * Prepares this object for garbage collection.
		 */
		public function destroy():void 
		{
			Tweener.removeTweens(this);
			
			removeFromParent(true);
		}
		
		
		//=================================
		//		PROTECTED METHODS
		//=================================
		
		/**
		 * Called when the user triggers the back button on this screen. Sends out a notification so the application can determine what to do next.
		 */
		protected function HandleTriggeredBack():void 
		{
			ObserverManager.notify(WitchWayEvent.USER_TRIGGERED_SCREEN_BACK, this);
		}
		
		
		//=================================
		//		ACCESSORS
		//=================================
		
		public function get startX():int 
		{
			return _startX;
		}
		
		public function set startX(value:int):void 
		{
			_startX = value;
		}
	}
}