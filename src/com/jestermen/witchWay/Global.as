package com.jestermen.witchWay 
{
	import flash.text.TextField;
	
	/**
	 * Contains properties and methods that are available to every class in "Witch Way."
	 * 
	 * @author Adam Testerman
	 */
	public class Global 
	{
		//PRIMITIVES
		public static var scale:Number;
		public static var isUsingCustomAnswersOnly:Boolean;
		public static var txtInputCustomAnswerStartScale:Number;
		
		//OBJECTS
		public static var txtInputCustomAnswer:TextField;
		public static var witchAnswers:XML;
	}
}